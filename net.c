#include "net.h"
#include <unistd.h>
#include <poll.h>

#define MS_IN_SEC 1000

int net_init(struct net_s *net)
{
	net->sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (net->sock < 0)
		goto err;

	net->addr_in.sin_family = AF_INET;
	net->addr_in.sin_port = htons(net->port);
	if (inet_pton(AF_INET, net->addr, &net->addr_in.sin_addr) <= 0)
		goto err;
	return 0;
err:
	return -1;
}

void net_done(struct net_s *net)
{
	close(net->sock);
}

int net_send(struct net_s *net, char *buf, size_t len)
{
	int ret;
	if (buf == NULL)
		return -1;
	ret = (int) sendto(net->sock, buf, len, 0,
					   (struct sockaddr*)&net->addr_in,
					   sizeof(struct sockaddr));
	return ret;
}

int net_recv(struct net_s *net, char *buf, size_t len)
{
	int ret;
	struct pollfd fd;
	if (buf == NULL)
		return -2;
	fd.fd = net->sock;
	fd.events = POLLIN;
	ret = poll (&fd, 1, net->recv_timeout * MS_IN_SEC);
	if (ret == -1)
		return -1;
	else if (ret == 0)
		return 0;

	return (int)recvfrom(net->sock, buf, len, 0, NULL, NULL);
}
