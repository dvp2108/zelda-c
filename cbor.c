#include "cbor.h"

#include "byteorder.h"

#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef MODULE_CBOR_CTIME
#include <time.h>
#endif

#define CBOR_TYPE_MASK          0xE0    /* top 3 bits */
#define CBOR_INFO_MASK          0x1F    /* low 5 bits */

#define CBOR_BYTE_FOLLOWS       24      /* indicator that the next byte is part of this item */

#define CBOR_UINT       0x00            /* type 0 */
#define CBOR_NEGINT     0x20            /* type 1 */
#define CBOR_BYTES      0x40            /* type 2 */
#define CBOR_TEXT       0x60            /* type 3 */
#define CBOR_ARRAY      0x80            /* type 4 */
#define CBOR_MAP        0xA0            /* type 5 */
#define CBOR_TAG        0xC0            /* type 6 */
#define CBOR_7          0xE0            /* type 7 (float and other types) */

#define CBOR_UINT8_FOLLOWS      24      /* 0x18 */
#define CBOR_UINT16_FOLLOWS     25      /* 0x19 */
#define CBOR_UINT32_FOLLOWS     26      /* 0x1a */
#define CBOR_UINT64_FOLLOWS     27      /* 0x1b */

#define CBOR_VAR_FOLLOWS        31      /* 0x1f */

#define CBOR_DATETIME_STRING_FOLLOWS        0
#define CBOR_DATETIME_EPOCH_FOLLOWS         1

#define CBOR_FALSE      (CBOR_7 | 20)
#define CBOR_TRUE       (CBOR_7 | 21)
#define CBOR_NULL       (CBOR_7 | 22)
#define CBOR_UNDEFINED  (CBOR_7 | 23)
#define CBOR_FLOAT16    (CBOR_7 | 25)
#define CBOR_FLOAT32    (CBOR_7 | 26)
#define CBOR_FLOAT64    (CBOR_7 | 27)
#define CBOR_BREAK      (CBOR_7 | 31)

#define CBOR_TYPE(stream, offset) (stream->data[offset] & CBOR_TYPE_MASK)
#define CBOR_ADDITIONAL_INFO(stream, offset) (stream->data[offset] & CBOR_INFO_MASK)

#define CBOR_ENSURE_SIZE(stream, bytes) do { \
    if (stream->pos + bytes >= stream->size) { return 0; } \
} while(0)

#define CBOR_ENSURE_SIZE_READ(stream, bytes) do { \
    if (bytes > stream->size) { return 0; } \
} while(0)

#define CBOR_STREAM_PRINT_BUFFERSIZE 1024 /* bytes */

#define MAX_TIMESTRING_LENGTH   (21)

#ifndef INFINITY
#define INFINITY (1.0/0.0)
#endif
#ifndef NAN
#define NAN (0.0/0.0)
#endif


#pragma GCC diagnostic error "-Wcast-align"
typedef struct __attribute__((packed)) {
    unsigned char u8;
    union {
        uint16_t u16;
        uint32_t u32;
        uint64_t u64;
    } u;
} cast_align_u8_t;

#ifdef MODULE_CBOR_FLOAT

static uint32_t htonf(float x)
{
    union u {
        float f;
        uint32_t i;
    } u = { .f = x };
    return htonl(u.i);
}

static float ntohf(uint32_t x)
{
    union u {
        float f;
        uint32_t i;
    } u = { .i = ntohl(x) };
    return u.f;
}

static uint64_t htond(double x)
{
    union u {
        double d;
        uint64_t i;
    } u = { .d = x };
    return htonll(u.i);
}

static double ntohd(uint64_t x)
{
    union u {
        double d;
        uint64_t i;
    } u = { .i = htonll(x) };
    return u.d;
}

double decode_float_half(unsigned char *halfp)
{
    int half = (halfp[0] << 8) + halfp[1];
    int exp = (half >> 10) & 0x1f;
    int mant = half & 0x3ff;
    double val;

    if (exp == 0) {
        val = ldexp(mant, -24);
    }
    else if (exp != 31) {
        val = ldexp(mant + 1024, exp - 25);
    }
    else {
        val = mant == 0 ? INFINITY : NAN;
    }

    return (half & 0x8000) ? -val : val;
}

static uint16_t encode_float_half(float x)
{
    union u {
        float f;
        uint32_t i;
    } u = { .f = x };

    uint16_t bits = (u.i >> 16) & 0x8000; /* Get the sign */
    uint16_t m = (u.i >> 12) & 0x07ff; /* Keep one extra bit for rounding */
    unsigned int e = (u.i >> 23) & 0xff; /* Using int is faster here */

    if (e < 103) {
        return bits;
    }

    if (e > 142) {
        bits |= 0x7c00u;
        bits |= (e == 255) && (u.i & 0x007fffffu);
        return bits;
    }

    if (e < 113) {
        m |= 0x0800u;
        bits |= (m >> (114 - e)) + ((m >> (113 - e)) & 1);
        return bits;
    }

    bits |= ((e - 112) << 10) | (m >> 1);
    bits += m & 1;
    return bits;
}
#endif /* MODULE_CBOR_FLOAT */

void dump_memory(const unsigned char *data, size_t size)
{
    if (!data || !size) {
        return;
    }

    printf("0x");

    for (size_t i = 0; i < size; ++i) {
        printf("%02X", data[i]);
    }
    puts("");
}

void cbor_init(cbor_stream_t *stream, unsigned char *buffer, size_t size)
{
    if (!stream) {
        return;
    }

    stream->data = buffer;
    stream->size = size;
    stream->pos = 0;
}

void cbor_clear(cbor_stream_t *stream)
{
    if (!stream) {
        return;
    }

    stream->pos = 0;
}

void cbor_destroy(cbor_stream_t *stream)
{
    if (!stream) {
        return;
    }

    stream->data = 0;
    stream->size = 0;
    stream->pos = 0;
}

static unsigned char uint_additional_info(uint64_t val)
{
    if (val < CBOR_UINT8_FOLLOWS) {
        return (unsigned char)val;
    }
    else if (val <= 0xff) {
        return CBOR_UINT8_FOLLOWS;
    }
    else if (val <= 0xffff) {
        return CBOR_UINT16_FOLLOWS;
    }
    else if (val <= 0xffffffffL) {
        return CBOR_UINT32_FOLLOWS;
    }

    return CBOR_UINT64_FOLLOWS;
}

static unsigned char uint_bytes_follow(unsigned char additional_info)
{
    if (additional_info < CBOR_UINT8_FOLLOWS || additional_info > CBOR_UINT64_FOLLOWS) {
        return 0;
    }

    static const unsigned char BYTES_FOLLOW[] = {1, 2, 4, 8};
    return BYTES_FOLLOW[additional_info - CBOR_UINT8_FOLLOWS];
}

static size_t encode_int(unsigned char major_type, cbor_stream_t *s, uint64_t val)
{
    if (!s) {
        return 0;
    }

    unsigned char additional_info = uint_additional_info(val);
    unsigned char bytes_follow = uint_bytes_follow(additional_info);
    CBOR_ENSURE_SIZE(s, bytes_follow + 1);
    s->data[s->pos++] = major_type | additional_info;

    for (int i = bytes_follow - 1; i >= 0; --i) {
        s->data[s->pos++] = (val >> (8 * i)) & 0xff;
    }

    return (size_t)bytes_follow + 1;
}

static size_t decode_int(const cbor_stream_t *s, size_t offset, uint64_t *val)
{
    if (!s) {
        return 0;
    }

    *val = 0; /* clear val first */

    CBOR_ENSURE_SIZE_READ(s, offset + 1);

    unsigned char *in = &s->data[offset];
    unsigned char additional_info = CBOR_ADDITIONAL_INFO(s, offset);
    unsigned char bytes_follow = uint_bytes_follow(additional_info);

    CBOR_ENSURE_SIZE_READ(s, offset + 1 + bytes_follow);

    switch (bytes_follow) {
        case 0:
            *val = (in[0] & CBOR_INFO_MASK);
            break;

        case 1:
            *val = in[1];
            break;

        case 2:
            *val = htons(((cast_align_u8_t *)in)->u.u16);
            break;

        case 4:
            *val = htonl(((cast_align_u8_t *)in)->u.u32);
            break;

        default:
            *val = htonll(((cast_align_u8_t *)in)->u.u64);
            break;
    }

    return (size_t)bytes_follow + 1;
}

static size_t encode_bytes(unsigned char major_type, cbor_stream_t *s, const char *data,
                           size_t length)
{
    size_t length_field_size = (size_t)uint_bytes_follow(uint_additional_info(length)) + 1;
    CBOR_ENSURE_SIZE(s, length_field_size + length);

    size_t bytes_start = encode_int(major_type, s, (uint64_t) length);

    if (!bytes_start) {
        return 0;
    }

    memcpy(&(s->data[s->pos]), data, length); /* copy byte string into our cbor struct */
    s->pos += length;
    return (bytes_start + length);
}

static size_t decode_bytes(const cbor_stream_t *s, size_t offset, char *out, size_t length)
{
    CBOR_ENSURE_SIZE_READ(s, offset + 1);

    if ((CBOR_TYPE(s, offset) != CBOR_BYTES && CBOR_TYPE(s, offset) != CBOR_TEXT) || !out) {
        return 0;
    }

    uint64_t bytes_length;
    size_t bytes_start = decode_int(s, offset, &bytes_length);

    if (!bytes_start) {
        return 0;
    }

    if (bytes_length == SIZE_MAX || length < bytes_length + 1) {
        return 0;
    }

    CBOR_ENSURE_SIZE_READ(s, offset + bytes_start + bytes_length);

    memcpy(out, &s->data[offset + bytes_start], bytes_length);
    out[bytes_length] = '\0';
    return (bytes_start + bytes_length);
}

static size_t decode_bytes_no_copy(const cbor_stream_t *s, size_t offset, unsigned char **out, size_t *length)
{
    CBOR_ENSURE_SIZE_READ(s, offset + 1);

    if ((CBOR_TYPE(s, offset) != CBOR_BYTES && CBOR_TYPE(s, offset) != CBOR_TEXT) || !out) {
        return 0;
    }

    uint64_t bytes_length;
    size_t bytes_start = decode_int(s, offset, &bytes_length);

    if (!bytes_start) {
        return 0;
    }

    CBOR_ENSURE_SIZE_READ(s, offset + bytes_start + bytes_length);
    *out = &(s->data[offset + bytes_start]);
    *length = bytes_length;
    return (bytes_start + bytes_length);
}

size_t cbor_deserialize_int(const cbor_stream_t *stream, size_t offset, int *val)
{
    CBOR_ENSURE_SIZE_READ(stream, offset + 1);

    if ((CBOR_TYPE(stream, offset) != CBOR_UINT && CBOR_TYPE(stream, offset) != CBOR_NEGINT) || !val) {
        return 0;
    }

    uint64_t buf;
    size_t read_bytes = decode_int(stream, offset, &buf);

    if (!read_bytes) {
        return 0;
    }

    if (CBOR_TYPE(stream, offset) == CBOR_UINT) {
        *val = (int)buf; /* resolve as CBOR_UINT */
    }
    else {
        *val = (int)(-1 - (int)buf); /* resolve as CBOR_NEGINT */
    }

    return read_bytes;
}

size_t cbor_serialize_int(cbor_stream_t *s, int val)
{
    if (val >= 0) {
        /* Major type 0: an unsigned integer */
        return encode_int(CBOR_UINT, s, (uint64_t)val);
    }
    else {
        /* Major type 1: an negative integer */
        return encode_int(CBOR_NEGINT, s, (uint64_t)(-1 - val));
    }
}

size_t cbor_deserialize_uint64_t(const cbor_stream_t *stream, size_t offset, uint64_t *val)
{
    if (CBOR_TYPE(stream, offset) != CBOR_UINT || !val) {
        return 0;
    }

    return decode_int(stream, offset, val);
}

size_t cbor_serialize_uint64_t(cbor_stream_t *s, uint64_t val)
{
    return encode_int(CBOR_UINT, s, val);
}

size_t cbor_deserialize_int64_t(const cbor_stream_t *stream, size_t offset, int64_t *val)
{
    if ((CBOR_TYPE(stream, offset) != CBOR_UINT && CBOR_TYPE(stream, offset) != CBOR_NEGINT) || !val) {
        return 0;
    }

    uint64_t buf;
    size_t read_bytes = decode_int(stream, offset, &buf);

    if (CBOR_TYPE(stream, offset) == CBOR_UINT) {
        *val = (int64_t)buf; /* resolve as CBOR_UINT */
    }
    else {
        *val = (-1 - (int64_t)buf); /* resolve as CBOR_NEGINT */
    }

    return read_bytes;
}

size_t cbor_serialize_int64_t(cbor_stream_t *s, int64_t val)
{
    if (val >= 0) {
        /* Major type 0: an unsigned integer */
        return encode_int(CBOR_UINT, s, (uint64_t)val);
    }
    else {
        /* Major type 1: an negative integer */
        return encode_int(CBOR_NEGINT, s, (uint64_t)(-1 - val));
    }
}

size_t cbor_deserialize_bool(const cbor_stream_t *stream, size_t offset, bool *val)
{
    if (CBOR_TYPE(stream, offset) != CBOR_7 || !val) {
        return 0;
    }

    unsigned char byte = stream->data[offset];
    *val = (byte == CBOR_TRUE);
    return 1;
}

size_t cbor_serialize_bool(cbor_stream_t *s, bool val)
{
    CBOR_ENSURE_SIZE(s, 1);
    s->data[s->pos++] = val ? CBOR_TRUE : CBOR_FALSE;
    return 1;
}

#ifdef MODULE_CBOR_FLOAT
size_t cbor_deserialize_float_half(const cbor_stream_t *stream, size_t offset, float *val)
{
    if (CBOR_TYPE(stream, offset) != CBOR_7 || !val) {
        return 0;
    }

    unsigned char *data = &stream->data[offset];

    if (*data == CBOR_FLOAT16) {
        *val = (float)decode_float_half(data + 1);
        return 3;
    }

    return 0;
}

size_t cbor_serialize_float_half(cbor_stream_t *s, float val)
{
    CBOR_ENSURE_SIZE(s, 3);
    s->data[s->pos++] = CBOR_FLOAT16;
    uint16_t encoded_val = htons(encode_float_half(val));
    memcpy(s->data + s->pos, &encoded_val, 2);
    s->pos += 2;
    return 3;
}

size_t cbor_deserialize_float(const cbor_stream_t *stream, size_t offset, float *val)
{
    if (CBOR_TYPE(stream, offset) != CBOR_7 || !val) {
        return 0;
    }

    unsigned char *data = &stream->data[offset];

    if (*data == CBOR_FLOAT32) {
        *val = ntohf(((cast_align_u8_t *)data)->u.u32);
        return 5;
    }

    return 0;
}

size_t cbor_serialize_float(cbor_stream_t *s, float val)
{
    CBOR_ENSURE_SIZE(s, 5);
    s->data[s->pos++] = CBOR_FLOAT32;
    uint32_t encoded_val = htonf(val);
    memcpy(s->data + s->pos, &encoded_val, 4);
    s->pos += 4;
    return 5;
}

size_t cbor_deserialize_double(const cbor_stream_t *stream, size_t offset, double *val)
{
    CBOR_ENSURE_SIZE_READ(stream, offset + 1);

    if (CBOR_TYPE(stream, offset) != CBOR_7 || !val) {
        return 0;
    }

    unsigned char *data = &stream->data[offset];

    if (*data == CBOR_FLOAT64) {
        CBOR_ENSURE_SIZE_READ(stream, offset + 9);
        *val = ntohd(((cast_align_u8_t *)data)->u.u64);
        return 9;
    }

    return 0;
}

size_t cbor_serialize_double(cbor_stream_t *s, double val)
{
    CBOR_ENSURE_SIZE(s, 9);
    s->data[s->pos++] = CBOR_FLOAT64;
    uint64_t encoded_val = htond(val);
    memcpy(s->data + s->pos, &encoded_val, 8);
    s->pos += 8;
    return 9;
}
#endif /* MODULE_CBOR_FLOAT */

size_t cbor_deserialize_byte_string(const cbor_stream_t *stream, size_t offset, char *val,
                                    size_t length)
{
    CBOR_ENSURE_SIZE_READ(stream, offset + 1);

    if (CBOR_TYPE(stream, offset) != CBOR_BYTES) {
        return 0;
    }

    return decode_bytes(stream, offset, val, length);
}

size_t cbor_deserialize_byte_string_no_copy(const cbor_stream_t *stream, size_t offset, unsigned char **val,
                                    size_t *length)
{
    CBOR_ENSURE_SIZE_READ(stream, offset + 1);

    if (CBOR_TYPE(stream, offset) != CBOR_BYTES) {
        return 0;
    }

    return decode_bytes_no_copy(stream, offset, val, length);
}

size_t cbor_serialize_byte_string(cbor_stream_t *stream, const char *val)
{
    return encode_bytes(CBOR_BYTES, stream, val, strlen(val));
}

size_t cbor_serialize_byte_stringl(cbor_stream_t *stream, const char *val, size_t length)
{
    return encode_bytes(CBOR_BYTES, stream, val, length);
}

size_t cbor_deserialize_unicode_string(const cbor_stream_t *stream, size_t offset, char *val,
                                       size_t length)
{
    CBOR_ENSURE_SIZE_READ(stream, offset + 1);

    if (CBOR_TYPE(stream, offset) != CBOR_TEXT) {
        return 0;
    }

    return decode_bytes(stream, offset, val, length);
}

size_t cbor_deserialize_unicode_string_no_copy(const cbor_stream_t *stream, size_t offset, unsigned char **val,
                                    size_t *length)
{
    CBOR_ENSURE_SIZE_READ(stream, offset + 1);

    if (CBOR_TYPE(stream, offset) != CBOR_TEXT) {
        return 0;
    }

    return decode_bytes_no_copy(stream, offset, val, length);
}

size_t cbor_serialize_unicode_string(cbor_stream_t *stream, const char *val)
{
    return encode_bytes(CBOR_TEXT, stream, val, strlen(val));
}

size_t cbor_deserialize_array(const cbor_stream_t *s, size_t offset, size_t *array_length)
{
    if (CBOR_TYPE(s, offset) != CBOR_ARRAY || !array_length) {
        return 0;
    }

    uint64_t val;
    size_t read_bytes = decode_int(s, offset, &val);
    *array_length = (size_t)val;
    return read_bytes;
}

size_t cbor_serialize_array(cbor_stream_t *s, size_t array_length)
{
    /* serialize number of array items */
    return encode_int(CBOR_ARRAY, s, array_length);
}

size_t cbor_serialize_array_indefinite(cbor_stream_t *s)
{
    CBOR_ENSURE_SIZE(s, 1);
    s->data[s->pos++] = CBOR_ARRAY | CBOR_VAR_FOLLOWS;
    return 1;

}

size_t cbor_deserialize_array_indefinite(const cbor_stream_t *s, size_t offset)
{
    if (s->data[offset] != (CBOR_ARRAY | CBOR_VAR_FOLLOWS)) {
        return 0;
    }

    return 1;
}

size_t cbor_serialize_map_indefinite(cbor_stream_t *s)
{
    CBOR_ENSURE_SIZE(s, 1);
    s->data[s->pos++] = CBOR_MAP | CBOR_VAR_FOLLOWS;
    return 1;
}

size_t cbor_deserialize_map_indefinite(const cbor_stream_t *s, size_t offset)
{
    if (s->data[offset] != (CBOR_MAP | CBOR_VAR_FOLLOWS)) {
        return 0;
    }

    return 1;
}

size_t cbor_deserialize_map(const cbor_stream_t *s, size_t offset, size_t *map_length)
{
    if (CBOR_TYPE(s, offset) != CBOR_MAP || !map_length) {
        return 0;
    }

    uint64_t val;
    size_t read_bytes = decode_int(s, offset, &val);
    *map_length = (size_t)val;
    return read_bytes;
}

size_t cbor_serialize_map(cbor_stream_t *s, size_t map_length)
{
    /* serialize number of item key-value pairs */
    return encode_int(CBOR_MAP, s, map_length);
}

#ifdef MODULE_CBOR_SEMANTIC_TAGGING
#ifdef MODULE_CBOR_CTIME
size_t cbor_deserialize_date_time(const cbor_stream_t *stream, size_t offset, struct tm *val)
{
    if ((CBOR_TYPE(stream, offset) != CBOR_TAG)
        || (CBOR_ADDITIONAL_INFO(stream, offset) != CBOR_DATETIME_STRING_FOLLOWS)) {
        return 0;
    }

    char buffer[21];
    offset++;  /* skip tag byte to decode date_time */
    size_t read_bytes = cbor_deserialize_unicode_string(stream, offset, buffer, sizeof(buffer));
    const char *format = "%Y-%m-%dT%H:%M:%SZ";

    if (strptime(buffer, format, val) == 0) {
        return 0;
    }

    val->tm_isdst = -1; /* not set by strptime(), undefined in CBOR */

    if (mktime(val) == -1) {
        return 0;
    }

    return read_bytes + 1;  /* + 1 tag byte */
}

size_t cbor_serialize_date_time(cbor_stream_t *stream, struct tm *val)
{
    CBOR_ENSURE_SIZE(stream, MAX_TIMESTRING_LENGTH + 1); /* + 1 tag byte */

    char time_str[MAX_TIMESTRING_LENGTH];
    const char *format = "%Y-%m-%dT%H:%M:%SZ";

    if (strftime(time_str, sizeof(time_str), format, val) == 0) { /* struct tm to string */
        return 0;
    }

    if (!cbor_write_tag(stream, CBOR_DATETIME_STRING_FOLLOWS)) {
        return 0;
    }

    size_t written_bytes = cbor_serialize_unicode_string(stream, time_str);
    return written_bytes + 1; /* utf8 time string length + tag length */
}

size_t cbor_deserialize_date_time_epoch(const cbor_stream_t *stream, size_t offset, time_t *val)
{
    if ((CBOR_TYPE(stream, offset) != CBOR_TAG)
        || (CBOR_ADDITIONAL_INFO(stream, offset) != CBOR_DATETIME_EPOCH_FOLLOWS)) {
        return 0;
    }

    offset++; /* skip tag byte */
    uint64_t epoch;
    size_t read_bytes = cbor_deserialize_uint64_t(stream, offset, &epoch);

    if (!read_bytes) {
        return 0;
    }

    *val = (time_t)epoch;
    return read_bytes + 1; /* + 1 tag byte */
}

size_t cbor_serialize_date_time_epoch(cbor_stream_t *stream, time_t val)
{
    /* we need at least 2 bytes (tag byte + at least 1 byte for the integer) */
    CBOR_ENSURE_SIZE(stream, 2);

    if (val < 0) {
        return 0; /* we currently don't support negative values for the time_t object */
    }

    if (!cbor_write_tag(stream, CBOR_DATETIME_EPOCH_FOLLOWS)) {
        return 0;
    }


    uint64_t time = (uint64_t)val;
    size_t written_bytes = encode_int(CBOR_UINT, stream, time);
    return written_bytes + 1; /* + 1 tag byte */
}
#endif /* MODULE_CBOR_CTIME */


size_t cbor_write_tag(cbor_stream_t *s, unsigned char tag)
{
    CBOR_ENSURE_SIZE(s, 1);
    s->data[s->pos++] = CBOR_TAG | tag;
    return 1;
}

bool cbor_at_tag(const cbor_stream_t *s, size_t offset)
{
    return cbor_at_end(s, offset) || CBOR_TYPE(s, offset) == CBOR_TAG;
}
#endif /* MODULE_CBOR_SEMANTIC_TAGGING */

size_t cbor_write_break(cbor_stream_t *s)
{
    CBOR_ENSURE_SIZE(s, 1);
    s->data[s->pos++] = CBOR_BREAK;
    return 1;
}

bool cbor_at_break(const cbor_stream_t *s, size_t offset)
{
    return cbor_at_end(s, offset) || s->data[offset] == CBOR_BREAK;
}

bool cbor_at_end(const cbor_stream_t *s, size_t offset)
{
    return s ? offset >= s->pos - 1 : true;
}

void cbor_stream_print(const cbor_stream_t *stream)
{
    dump_memory(stream->data, stream->pos);
}

static size_t cbor_stream_decode_skip(cbor_stream_t *stream, size_t offset)
{
    size_t consume_bytes = 0;

    switch (CBOR_ADDITIONAL_INFO(stream, offset)) {
        case CBOR_BYTE_FOLLOWS:
            consume_bytes = 2;
            break;

        default:
            consume_bytes = 1;
            break;
    }

    printf("(unsupported, ");
    dump_memory(stream->data + offset, consume_bytes);
    puts(")");
    return consume_bytes;
}

static size_t cbor_stream_decode_at(cbor_stream_t *stream, size_t offset, int indent)
{
#define DESERIALIZE_AND_PRINT(type, suffix, format_string) { \
        type val; \
        size_t read_bytes = cbor_deserialize_##suffix(stream, offset, &val); \
        printf("("#type", "format_string")\n", val); \
        return read_bytes; \
    }

    printf("%*s", indent, "");

    switch (CBOR_TYPE(stream, offset)) {
        case CBOR_UINT:
            DESERIALIZE_AND_PRINT(uint64_t, uint64_t, "%" PRIu64)
        case CBOR_NEGINT:
            DESERIALIZE_AND_PRINT(int64_t, int64_t, "%" PRId64)
        case CBOR_BYTES: {
            char buffer[CBOR_STREAM_PRINT_BUFFERSIZE];
            size_t read_bytes = cbor_deserialize_byte_string(stream, offset, buffer, sizeof(buffer));
            printf("(byte string, \"%s\")\n", buffer);
            return read_bytes;
        }

        case CBOR_TEXT: {
            char buffer[CBOR_STREAM_PRINT_BUFFERSIZE];
            size_t read_bytes = cbor_deserialize_unicode_string(stream, offset, buffer, sizeof(buffer));
            printf("(unicode string, \"%s\")\n", buffer);
            return read_bytes;
        }

        case CBOR_ARRAY: {
            const bool is_indefinite = (stream->data[offset] == (CBOR_ARRAY | CBOR_VAR_FOLLOWS));
            uint64_t array_length = 0;
            size_t read_bytes;

            if (is_indefinite) {
                offset += read_bytes = cbor_deserialize_array_indefinite(stream, offset);
                puts("(array, length: [indefinite])");
            }
            else {
                offset += read_bytes = decode_int(stream, offset, &array_length);
                printf("(array, length: %"PRIu64")\n", array_length);
            }

            size_t i = 0;

            while (is_indefinite ? !cbor_at_break(stream, offset) : i < array_length) {
                size_t inner_read_bytes;
                offset += inner_read_bytes = cbor_stream_decode_at(stream, offset, indent + 2);

                if (inner_read_bytes == 0) {
                    break;
                }

                read_bytes += inner_read_bytes;
                ++i;
            }

            read_bytes += cbor_at_break(stream, offset);
            return read_bytes;
        }

        case CBOR_MAP: {
            const bool is_indefinite = (stream->data[offset] == (CBOR_MAP | CBOR_VAR_FOLLOWS));
            uint64_t map_length = 0;
            size_t read_bytes;

            if (is_indefinite) {
                offset += read_bytes = cbor_deserialize_map_indefinite(stream, offset);
                puts("(map, length: [indefinite])");
            }
            else {
                offset += read_bytes = decode_int(stream, offset, &map_length);
                printf("(map, length: %"PRIu64")\n", map_length);
            }

            size_t i = 0;

            while (is_indefinite ? !cbor_at_break(stream, offset) : i < map_length) {
                size_t key_read_bytes, value_read_bytes;
                offset += key_read_bytes = cbor_stream_decode_at(stream, offset, indent + 1); /* key */
                offset += value_read_bytes = cbor_stream_decode_at(stream, offset, indent + 2); /* value */

                if (key_read_bytes == 0 || value_read_bytes == 0) {
                    break;
                }

                read_bytes += key_read_bytes + value_read_bytes;
                ++i;
            }

            read_bytes += cbor_at_break(stream, offset);
            return read_bytes;
        }
#ifdef MODULE_CBOR_SEMANTIC_TAGGING
        case CBOR_TAG: {
            unsigned char tag = CBOR_ADDITIONAL_INFO(stream, offset);

            switch (tag) {
#ifdef MODULE_CBOR_CTIME
                case CBOR_DATETIME_STRING_FOLLOWS: {
                    char buf[64];
                    struct tm timeinfo;
                    size_t read_bytes = cbor_deserialize_date_time(stream, offset, &timeinfo);
                    strftime(buf, sizeof(buf), "%c", &timeinfo);
                    printf("(tag: %u, date/time string: \"%s\")\n", tag, buf);
                    return read_bytes;
                }

                case CBOR_DATETIME_EPOCH_FOLLOWS: {
                    time_t time;
                    size_t read_bytes = cbor_deserialize_date_time_epoch(stream, offset, &time);
                    printf("(tag: %u, date/time epoch: %d)\n", tag, (int)time);
                    return read_bytes;
                }

#endif /* MODULE_CBOR_CTIME */

                default:
                    break;
            }
            break;
        }
#endif /* MODULE_CBOR_SEMANTIC_TAGGING */

        case CBOR_7: {
            switch (stream->data[offset]) {
                case CBOR_FALSE:
                case CBOR_TRUE:
                    DESERIALIZE_AND_PRINT(bool, bool, "%d")
#ifdef MODULE_CBOR_FLOAT
                case CBOR_FLOAT16:
                    DESERIALIZE_AND_PRINT(float, float_half, "%f")
                case CBOR_FLOAT32:
                    DESERIALIZE_AND_PRINT(float, float, "%f")
                case CBOR_FLOAT64:
                    DESERIALIZE_AND_PRINT(double, double, "%lf")
#endif /* MODULE_CBOR_FLOAT */
                default:
                    break;
            }
        }
    }

    return cbor_stream_decode_skip(stream, offset);

#undef DESERIALIZE_AND_PRINT
}

void cbor_stream_decode(cbor_stream_t *stream)
{
    puts("Data:");
    size_t offset = 0;

    while (offset < stream->pos) {
        size_t read_bytes = cbor_stream_decode_at(stream, offset, 0);

        if (read_bytes == 0) {
            cbor_stream_print(stream);
            return;
        }

        offset += read_bytes;
    }

    puts("");
}

static void __cbor_dumps(cbor_stream_t *, json_object *);

static void 
__cbor_dumps_value(cbor_stream_t *stream, json_object *jobj)
{
    enum json_type type;
    type = json_object_get_type(jobj);
    switch (type) {
        case json_type_boolean:
            cbor_serialize_bool(stream, json_object_get_boolean(jobj));
            break;
        case json_type_double:
            cbor_serialize_int64_t(stream, (int64_t)json_object_get_double(jobj));
            break;
        case json_type_int:
            cbor_serialize_int(stream, json_object_get_int(jobj));
            break;
        case json_type_string:
            cbor_serialize_unicode_string(stream, json_object_get_string(jobj));
            break;
        default:
            break;
    }
}

static void __cbor_dumps_array(cbor_stream_t *stream, json_object *jobj, char *key) {
	enum json_type type;

	json_object *jarray = jobj;
	if(key) {
		json_object_object_get_ex(jobj, key, &jarray);
	}

	int arraylen = json_object_array_length(jarray);
	cbor_serialize_array(stream, (size_t)arraylen);

	json_object * jvalue;

	for (int i = 0; i < arraylen; i++){
		jvalue = json_object_array_get_idx(jarray, i);
		type = json_object_get_type(jvalue);
		if (type == json_type_array) {
			__cbor_dumps_array(stream, jvalue, NULL);
		} else if (type != json_type_object) {
			__cbor_dumps_value(stream, jvalue);
		} else {
			__cbor_dumps(stream, jvalue);
		}
	}
}

static void __cbor_dumps(cbor_stream_t *stream, json_object *jso) {
    enum json_type type;
    int num = json_object_object_length(jso);
    cbor_serialize_map(stream, (size_t)num);
    json_object_object_foreach(jso, key, val) {
        type = json_object_get_type(val);
        cbor_serialize_unicode_string(stream, key);
        switch (type) {
            case json_type_boolean: 
            case json_type_double: 
            case json_type_int: 
            case json_type_string:
                __cbor_dumps_value(stream, val);
                break;
            case json_type_object: {
                json_object *jint;
                json_object_object_get_ex(jso, key, &jint);
                __cbor_dumps(stream, jint);
                break;
            }
            case json_type_array:
                __cbor_dumps_array(stream, jso, key);
                break;
            default:
                break;
        }
    }
}

void cbor_dumps(cbor_stream_t *stream, json_object *jso)
{
    __cbor_dumps(stream, jso);
}

static json_object *__cbor_loads(cbor_stream_t *stream, size_t *offset)
{
    switch(CBOR_TYPE(stream, *offset)) {
        case CBOR_UINT: {
            uint64_t val;
            json_object *jso;
            size_t read_bytes = cbor_deserialize_uint64_t(stream, *offset, &val);
            if (read_bytes <= 0)
                return NULL;
            *offset += read_bytes;
            jso = json_object_new_int64((int64_t)val);
            return jso;
        }
        case CBOR_NEGINT: {
            int64_t val;
            json_object *jso;
            size_t  read_bytes = cbor_deserialize_int64_t(stream, *offset, &val);
            if (read_bytes <= 0)
                return NULL;
            *offset += read_bytes;
            jso = json_object_new_int64(val);
            return jso;
        }
        case CBOR_BYTES: {
            char buffer[CBOR_STREAM_PRINT_BUFFERSIZE];
            json_object *jso;
            size_t read_bytes = cbor_deserialize_byte_string(stream, *offset, buffer, sizeof(buffer));
            if (read_bytes <= 0)
                return NULL;
            *offset += read_bytes;
            jso = json_object_new_string(buffer);
            return jso;
        }
        case CBOR_TEXT: {
            char buffer[CBOR_STREAM_PRINT_BUFFERSIZE];
            json_object *jso;
            size_t read_bytes = cbor_deserialize_unicode_string(stream, *offset, buffer, sizeof(buffer));
            if (read_bytes <= 0)
                return NULL;
            *offset += read_bytes;
            jso = json_object_new_string(buffer);
            return jso;
        }
        case CBOR_ARRAY: {
            const bool is_indefinite = (stream->data[*offset] ==
                    (CBOR_ARRAY | CBOR_VAR_FOLLOWS));
            uint64_t array_length = 0;
            size_t read_bytes;
            if (is_indefinite) {
                *offset += read_bytes = cbor_deserialize_array_indefinite(stream, *offset);
            } else {
                *offset += read_bytes = decode_int(stream, *offset, &array_length);
            }

            json_object *jarr;
            jarr = json_object_new_array();
            size_t i = 0;
            while (is_indefinite ? !cbor_at_break(stream, *offset): i < array_length) {
                json_object *value;
                value = __cbor_loads(stream, offset);
                if (value == NULL) {
                    json_object_put(jarr);
                    return NULL;
                }

                json_object_array_add(jarr, value);
                ++i;
            }
            *offset += cbor_at_break(stream, *offset);
            return jarr;
        }
        case CBOR_MAP: {
            const bool is_indefinite = (stream->data[*offset] == 
                (CBOR_MAP | CBOR_VAR_FOLLOWS));
            uint64_t map_length = 0;
            json_object *jso;

            if (is_indefinite) {
                *offset += cbor_deserialize_map_indefinite(stream, *offset);
            } else {
                *offset += decode_int(stream, *offset, &map_length);
            }

            jso = json_object_new_object();
            size_t i = 0;
            while (is_indefinite ? !cbor_at_break(stream, *offset) : i < map_length) {
                json_object *key, *value;
                const char *c_key;
                key = __cbor_loads(stream, offset);
                value = __cbor_loads(stream, offset);
                if (key == NULL || value == NULL) {
                    if (key != NULL)
                        json_object_put(key);
                    if (value != NULL)
                        json_object_put(value);
                    json_object_put(jso);
                    return NULL;
                }
                c_key = json_object_get_string(key);
                json_object_object_add(jso, c_key, value);
                json_object_put(key);
                ++i;
            }
            *offset += cbor_at_break(stream, *offset);
            return jso;
        }
        default:
            break;
    }
    *offset += cbor_stream_decode_skip(stream, *offset);
    return NULL;
}

struct json_object *cbor_loads(cbor_stream_t *stream)
{
    struct json_object *jso;
    size_t offset = 0;
    jso = __cbor_loads(stream, &offset);
    if (json_object_get_type(jso) == json_type_object)
        return jso;
    if (jso != NULL)
        json_object_put(jso);
    return NULL;
}

