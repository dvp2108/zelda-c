#include <string.h>
#include "bile.h"
#include "strbuf.h"
#include "package.h"
#include "leever.h"
#include <syslog.h>

#define ALLOC_CALLOC(x, nr) \
    x = calloc(1, (nr * sizeof(*(x))))

#define FREE(x) if (x) free(x); x = NULL

#define MS_IN_S 1000

static void send_keep_alive(struct uloop_timeout *t)
{
    struct send_kend_alive_s *ska = container_of(t, struct send_kend_alive_s, u_timeout);
    json_object *jso;

    if (leever_empty()) {
        jso = get_ping();
        if (leever_push(jso)) {
            json_object_put(jso);
        }
    }
    uloop_timeout_set(t, ska->timeout * MS_IN_S);
}

static void __update(struct update_s *us)
{
    json_object *jso;
    jso = get_status();
    if (leever_push(jso)) {
        json_object_put(jso);
    }
}


static void update(struct uloop_timeout *t)
{
    struct update_s *us = container_of(t, struct update_s, u_timeout);

    __update(us);

    uloop_timeout_set(t, us->timeout * MS_IN_S);
}

static void statistics_handler(struct uloop_timeout *t)
{
    struct statistics_bile *stat_bile = container_of(t, struct statistics_bile, u_timeout);
    json_object *jso;
    jso = get_statistics(stat_bile->statistic->cmd);
    if (leever_push(jso)) {
        json_object_put(jso);
    }
    uloop_timeout_set(t, stat_bile->statistic->interval * MS_IN_S);
}

static void 
__network()
{
    json_object *jso;
    jso = get_interfaces();
    if (leever_push(jso)) {
        json_object_put(jso);
    }
}

static void 
event_network(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    __network();
}

static void
__routing()
{
    json_object *jso;
    jso = get_routing();
    if (leever_push(jso)) {
        json_object_put(jso);
    }
}

static void 
event_route(struct ubus_context *ctx, struct ubus_event_handler *ev,
            const char *type, struct blob_attr *msg)
{
    __routing();
}

static struct bile_s *bile = NULL;

static void statistics_init(struct config_s *config)
{
    struct statistic_s *stconf;
    struct statistics_bile *stbile;
    stconf = config->statistics;
    while (stconf) {
        stbile = calloc(1, sizeof(struct statistics_bile));
        stbile->statistic = stconf;
        stbile->u_timeout.cb = statistics_handler;
        uloop_timeout_set(&(stbile->u_timeout),
                          stbile->statistic->interval * MS_IN_S);

        stbile->next = bile->statistics;
        bile->statistics = stbile;

        stconf = stconf->next;
    }
}

static void statistics_done()
{
    struct statistics_bile *stbile;
    while(bile->statistics) {
        stbile = bile->statistics->next;
        FREE(bile->statistics);
        bile->statistics = stbile;
    }
}

int bile_init(struct config_s *config)
{
    int ret = 0;
    if (bile != NULL || !config)
        return (-1);
    ALLOC_CALLOC(bile, 1);

    bile->ska.u_timeout.cb = send_keep_alive;
    bile->ska.timeout = config->keep_alive_interval;
    uloop_timeout_set(&(bile->ska.u_timeout), bile->ska.timeout * MS_IN_S);

    bile->update.u_timeout.cb = update;
    bile->update.timeout = config->update_interval;
    uloop_timeout_set(&(bile->update.u_timeout), bile->update.timeout * MS_IN_S);

    bile->ctx = ubus_connect(bile->ubus_socket);
    if (!(bile->ctx)) {
        syslog(LOG_ERR, "could not connect with ubus");
        FREE(bile);
        return (-1);
    }
    bile->event_net.cb = event_network;
    bile->event_route.cb = event_route;

    ret = ubus_register_event_handler(bile->ctx, &bile->event_net, "network.interface");
    if (ret) {
        syslog(LOG_ERR, "not register network.interface");
        goto err;
    }

    ret = ubus_register_event_handler(bile->ctx, &bile->event_route, "defaultroute");
    if (ret) {
        syslog(LOG_ERR, "not unregister defaultroute");
        goto err;
    }

    bile->statistics = NULL;
    statistics_init(config);

    uloop_init();
    ubus_add_uloop(bile->ctx);
    __update(&bile->update);
    __network();
    __routing();
    return 0;
err:
    ubus_free(bile->ctx);
    FREE(bile);
    return ret;
}

static void *__bile_run()
{
    uloop_run();
}

void bile_run()
{
    pthread_create(&bile->pbile, NULL, (void*)__bile_run, NULL);
}

int bile_done()
{
    if(!bile)
        return (-1);
    uloop_done();
    ubus_free(bile->ctx);
    pthread_cancel(bile->pbile);
    pthread_join(bile->pbile, NULL);
    statistics_done();
    FREE(bile);
    return 0;
}
