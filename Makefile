CFLAGS = -I.
LDFLAGS = -ljson-c -lpthread -luci -lubox -lubus
OBJ:= zelda.o bile.o cbor.o strbuf.o net.o package.o leever.o queue.o config.o

all: zelda

zelda: $(OBJ)
	$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS)

clean:
	rm zelda *.o

