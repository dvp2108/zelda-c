//
// Created by dmitry on 18.11.17.
//

#include "config.h"
#include <uci.h>
#include <uci_blob.h>

#define DEFAULT_KEEP_ALIVE_INTERVAL 60
#define DEFAULT_UPDATE_INTERVAL 20
#define DEFAULT_RECV_TIMEOUT 10
#define DEFAULT_RESEND_ATTEMPTS 10
#define DEFAULT_PORT 11000

void
__default_config (struct config_s *config)
{
    config->enable = 0;
    config->server = NULL;
    config->port = DEFAULT_PORT;
    config->recv_timeout = DEFAULT_RECV_TIMEOUT;
    config->resend_attempts = DEFAULT_RESEND_ATTEMPTS;
    config->update_interval = DEFAULT_UPDATE_INTERVAL;
    config->keep_alive_interval = DEFAULT_KEEP_ALIVE_INTERVAL;
    config->statistics = NULL;
}

int load_config(struct config_s *config)
{
    int ret = 0;
    struct uci_ptr ptr;
    struct uci_context *ctx = uci_alloc_context();
    struct uci_element *s_element, *o_element;
    struct uci_option *option;
    struct uci_section *section;

    __default_config(config);
    if (!ctx)
        return (-1);

    if (uci_lookup_ptr(ctx, &ptr, "zelda", true) != UCI_OK) {
        return (-1);
    }

    if (ptr.last->type != UCI_TYPE_PACKAGE) {
        ret = -1;
        goto out;
    }
    uci_foreach_element(&ptr.p->sections, s_element) {
        section = uci_to_section(s_element);
        if (!strcmp(section->type, "link")) {
            uci_foreach_element(&section->options, o_element) {
                option = uci_to_option(o_element);
                if (!strcmp(option->e.name, "enable")) {
                    config->enable = atoi(option->v.string);
                } else if (!strcmp(option->e.name, "server")) {
                    config->server = strdup(option->v.string);
                } else if (!strcmp(option->e.name, "port")) {
                    config->port = atoi(option->v.string);
                } else if (!strcmp(option->e.name, "recv_timeout")) {
                    config->recv_timeout = atoi(option->v.string);
                } else if (!strcmp(option->e.name, "resend_attempts")) {
                    config->resend_attempts = atoi(option->v.string);
                } else if (!strcmp(option->e.name, "update_interval")) {
                    config->update_interval = atoi(option->v.string);
                } else if (!strcmp(option->e.name, "keep_alive_interval")) {
                    config->keep_alive_interval = atoi(option->v.string);
                }
            }
        } else if (!strcmp(section->type, "statistic")) {
            int interval = -1;
            char *command = NULL;
            uci_foreach_element(&section->options, o_element) {
                option = uci_to_option(o_element);
                if (!strcmp(option->e.name, "interval")) {
                    interval = atoi(option->v.string);
                } else if (!strcmp(option->e.name, "command")) {
                    command = strdup(option->v.string);
                }
            }
            if (interval > 0 && command != NULL) {
                struct statistic_s *statistic;
                statistic = calloc(1, sizeof(struct statistic_s));
                statistic->next = config->statistics;
                statistic->cmd = command;
                statistic->interval = interval;
                config->statistics = statistic;
            } else {
                if (command)
                    free(command);
            }
        }
    }

    out:
    uci_unload(ctx, ptr.p);
    uci_free_context(ctx);
    return ret;
}

int free_config(struct config_s *config)
{
    struct statistic_s *t;
    if (config->server)
        free(config->server);

    while(config->statistics) {
        t = config->statistics->next;
        free(config->statistics->cmd);
        free(config->statistics);
        config->statistics = t;
    }

    return 0;
}
