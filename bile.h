#ifndef  ZELDA_C_BILE_H
#define ZELDA_C_BILE_H

#include <libubox/uloop.h>
#include <libubus.h>
#include "config.h"

struct send_kend_alive_s {
    struct uloop_timeout u_timeout;
    int timeout;
};

struct update_s {
    struct uloop_timeout u_timeout;
    int timeout;
};

struct statistics_bile {
    struct uloop_timeout u_timeout;
    struct statistic_s *statistic;
    struct statistics_bile *next;
};

struct bile_s {
    pthread_t pbile;
    struct send_kend_alive_s ska;
    struct update_s update;
    struct ubus_context *ctx;
    const char *ubus_socket;
    struct ubus_event_handler event_net;
    struct ubus_event_handler event_route;
    struct statistics_bile *statistics;
};

int bile_init(struct config_s*);

void bile_run();

int bile_done();

#endif
