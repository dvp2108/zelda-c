#ifndef CBOR_H
#define CBOR_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <json-c/json.h>
#ifdef MODULE_CBOR_CTIME
#include <time.h>
#endif /* MODULE_CBOR_CTIME */

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    unsigned char *data;    /**< Array containing CBOR encoded data */
    size_t size;            /**< Size of the array */
    size_t pos;             /**< Index to the next free byte */
} cbor_stream_t;

void cbor_init(cbor_stream_t *stream, unsigned char *buffer, size_t size);

void cbor_clear(cbor_stream_t *stream);

void cbor_destroy(cbor_stream_t *stream);

void cbor_stream_print(const cbor_stream_t *stream);

void cbor_stream_decode(cbor_stream_t *stream);

size_t cbor_serialize_int(cbor_stream_t *stream, int val);

size_t cbor_deserialize_int(const cbor_stream_t *stream, size_t offset,
                            int *val);

size_t cbor_serialize_uint64_t(cbor_stream_t *stream, uint64_t val);

size_t cbor_deserialize_uint64_t(const cbor_stream_t *stream, size_t offset,
                                 uint64_t *val);

size_t cbor_serialize_int64_t(cbor_stream_t *stream, int64_t val);

size_t cbor_deserialize_int64_t(const cbor_stream_t *stream, size_t offset,
                                int64_t *val);

size_t cbor_serialize_bool(cbor_stream_t *stream, bool val);

size_t cbor_deserialize_bool(const cbor_stream_t *stream, size_t offset,
                             bool *val);

#ifdef MODULE_CBOR_FLOAT
size_t cbor_serialize_float_half(cbor_stream_t *stream, float val);

size_t cbor_deserialize_float_half(const cbor_stream_t *stream, size_t offset,
                                   float *val);

size_t cbor_serialize_float(cbor_stream_t *stream, float val);

size_t cbor_deserialize_float(const cbor_stream_t *stream, size_t offset,
                              float *val);

size_t cbor_serialize_double(cbor_stream_t *stream, double val);

size_t cbor_deserialize_double(const cbor_stream_t *stream, size_t offset,
                               double *val);
#endif /* MODULE_CBOR_FLOAT */

size_t cbor_serialize_byte_string(cbor_stream_t *stream, const char *val);

size_t cbor_serialize_byte_stringl(cbor_stream_t *stream, const char *val, size_t length);

size_t cbor_deserialize_byte_string(const cbor_stream_t *stream, size_t offset,
                                    char *val, size_t length);

size_t cbor_serialize_unicode_string(cbor_stream_t *stream, const char *val);

size_t cbor_deserialize_byte_string_no_copy(const cbor_stream_t *stream, size_t offset,
                                            unsigned char **val, size_t *length);

size_t cbor_deserialize_unicode_string_no_copy(const cbor_stream_t *stream, size_t offset,
                                               unsigned char **val, size_t *length);

size_t cbor_deserialize_unicode_string(const cbor_stream_t *stream,
                                       size_t offset, char *val, size_t length);

size_t cbor_serialize_array(cbor_stream_t *stream, size_t array_length);

size_t cbor_deserialize_array(const cbor_stream_t *stream, size_t offset,
                              size_t *array_length);

size_t cbor_serialize_array_indefinite(cbor_stream_t *stream);

size_t cbor_deserialize_array_indefinite(const cbor_stream_t *stream, size_t offset);

size_t cbor_serialize_map(cbor_stream_t *stream, size_t map_length);

size_t cbor_deserialize_map(const cbor_stream_t *stream, size_t offset,
                            size_t *map_length);

size_t cbor_serialize_map_indefinite(cbor_stream_t *stream);

size_t cbor_deserialize_map_indefinite(const cbor_stream_t *stream, size_t offset);

#ifdef MODULE_CBOR_SEMANTIC_TAGGING
#ifdef MODULE_CBOR_CTIME
size_t cbor_serialize_date_time(cbor_stream_t *stream, struct tm *val);

size_t cbor_deserialize_date_time(const cbor_stream_t *stream, size_t offset, struct tm *val);

size_t cbor_serialize_date_time_epoch(cbor_stream_t *stream, time_t val);

size_t cbor_deserialize_date_time_epoch(const cbor_stream_t *stream, size_t offset, time_t *val);

#endif /* MODULE_CBOR_CTIME */

size_t cbor_write_tag(cbor_stream_t *stream, unsigned char tag);

bool cbor_at_tag(const cbor_stream_t *stream, size_t offset);

#endif /* MODULE_CBOR_SEMANTIC_TAGGING */

size_t cbor_write_break(cbor_stream_t *stream);

bool cbor_at_break(const cbor_stream_t *stream, size_t offset);

bool cbor_at_end(const cbor_stream_t *stream, size_t offset);

void cbor_dumps(cbor_stream_t *stream, json_object *jso);

struct json_object *cbor_loads(cbor_stream_t *stream);


#ifdef __cplusplus
}
#endif

#endif /* CBOR_H */
