#!/usr/bin/python2.7

import cbor
import socket
import json

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = '127.0.0.1'
server_port = 11002

server = (server_address, server_port)
sock.bind(server)
print("Listening on " + server_address + ":" + str(server_port))

msg = {"status": "accept"}
while True:
    data, client_address = sock.recvfrom(4096)
    try:
        payload = data.split('@')
        d = cbor.loads(payload[1])
        print(d)
    except Exception as e:
        print("error: " + str(e))
    sent = sock.sendto(cbor.dumps(msg), client_address)
