//
// Created by dmitry on 18.11.17.
//

#ifndef ZELDA_C_CONFIG_H
#define ZELDA_C_CONFIG_H

struct statistic_s {
    int interval;
    char *cmd;
    struct statistic_s *next;
};

struct config_s {
    int enable;
    char *server;
    int port;
    int recv_timeout;
    int resend_attempts;
    int update_interval;
    int keep_alive_interval;
    struct statistic_s *statistics;
};

int load_config(struct config_s *);

int free_config(struct config_s *);



#endif //ZELDA_C_CONFIG_H
