#include "leever.h"
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include "config.h"
#include "bile.h"

#define ALLOC_CALLOC(x, nr) \
    x = calloc(1, (nr * sizeof(*(x))))

#define FREE(x) if (x) free(x); x = NULL

static void usage()
{
    printf("Usage: zelda-c [<options>]\n");
    printf("Options\n");
    printf("-v:         More verbose output\n");
    printf("-d:         More debug output\n");
    printf("\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
    int i;
    struct config_s config;
    struct leever_s leever;
    struct strbuf sb;
    int verbose = 0;
    int debug = 0;

    for (i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
            usage();
        } else if (!strcmp(argv[i], "-d") || !strcmp(argv[i], "--debug")) {
            debug = 1;
        } else if (!strcmp(argv[i], "-v") || !strcmp(argv[i], "--verbose")) {
            verbose = 1;
        } else {
            usage();
        }
    }
    openlog("zeldad", LOG_PERROR | LOG_PID, LOG_DAEMON);
    if (debug) {
        setlogmask(LOG_MASK(LOG_DEBUG) | LOG_MASK(LOG_NOTICE)
                | LOG_MASK(LOG_WARNING) | LOG_MASK(LOG_ERR));
    } else if (verbose) {
        setlogmask(LOG_MASK(LOG_NOTICE)
                | LOG_MASK(LOG_WARNING) | LOG_MASK(LOG_ERR));
    } else {
        setlogmask(LOG_MASK(LOG_WARNING) | LOG_MASK(LOG_ERR));
    }
    if(load_config(&config)) {
        usage();
    }

    leever.resend_attempts = config.resend_attempts;
    leever.net.port = (uint16_t)config.port;
    leever.net.addr = config.server;
    leever.net.recv_timeout = config.recv_timeout;

    if(leever_init(&leever)) {
        usage();
    }

    strbuf_init(&sb, 0);
    strbuf_exec_cmd(&sb, "fw_printenv 2>/dev/null | awk -F= '/^serial/{print $2}'", 0);
    if(sb.buf)
        leever.id = strbuf_detach(&sb, (size_t*)&i);
    strbuf_release(&sb);

    if(bile_init(&config))
        goto end;

    bile_run();

    leever_loop();

end:
    bile_done();
    FREE(leever.id);
    leever_exit();
    free_config(&config);
    closelog();
    return 0;
}
