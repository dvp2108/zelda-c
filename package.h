#ifndef PACKAGE_H_
#define PACKAGE_H_
#include <json-c/json.h>

extern json_object *get_ping();

extern json_object *get_status();

extern json_object *get_interfaces();

extern json_object *get_routing();

extern json_object *get_statistics(char *cmd);

#endif
