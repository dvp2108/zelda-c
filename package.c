#include "package.h"
#include "strbuf.h"
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <syslog.h>

#define IN_P_DAY 86400

#define ALLOC_CALLOC(x, nr) \
    x = calloc(1, (nr * sizeof(*(x))))

#define FREE(x) if (x) free(x); x = NULL

#define JSON_ADD_OBJECT(x, key, val) \
    json_object_object_add(x, key, val)

#define JSON_ADD_INT(x, key, val) \
    JSON_ADD_OBJECT(x, key, json_object_new_int(val))

#define JSON_ADD_INT64(x, key, val) \
    JSON_ADD_OBJECT(x, key, json_object_new_double(val))

#define JSON_ADD_BOOLEAN(x, key, val) \
    JSON_ADD_OBJECT(x, key, json_object_new_boolean(val))

#define JSON_ADD_STRING(x, key, val) \
    JSON_ADD_OBJECT(x, key, json_object_new_string(val))

#define JSON_ADD_OBJECT_IN_ARRAY(x, val) \
    json_object_array_add(x, val);

#define JSON_ADD_STRING_IN_ARRAY(x, val) \
    JSON_ADD_OBJECT_IN_ARRAY(x, json_object_new_string(val))

static json_object*
ubus_call(char *cmd)
{
    struct strbuf sb;
    char buf[128] = "ubus call ";
    json_object *jso;
    strbuf_init(&sb, 0);
    strcat(buf, cmd);
    strbuf_exec_cmd(&sb, buf, 0);
    if (sb.len <= 0) {
        strbuf_release(&sb);
        syslog(LOG_NOTICE, "%s", buf);
        return NULL;
    }
    jso = json_tokener_parse(sb.buf);
    strbuf_release(&sb);
    if (json_object_get_type(jso) != json_type_object) {
        json_object_put(jso);
        jso = NULL;
        syslog(LOG_ERR, "parse in json %s", buf);
    }
    return jso;
}

json_object*
get_ping()
{
    json_object *jso_reply, *jso_data;
    jso_reply = json_object_new_object();
    jso_data = json_object_new_object();
    JSON_ADD_STRING(jso_reply, "type", "ping");
    JSON_ADD_OBJECT(jso_reply, "data", jso_data);
    return jso_reply;
}

static void 
__get_uptime(time_t uptime, char *val)
{
    struct tm *t;
    t = gmtime((time_t*)&uptime);
    int days = (int)(uptime/IN_P_DAY);
    if (days == 0)
        strftime(val, 40, "%H:%M:%S", t);
    else {
        sprintf(val, "%i days, ", days);
        strftime(val+strlen(val), 40-strlen(val), "%H:%M:%S", t);
    }
}

static void 
get_uptime(json_object *root, char *val)
{
    json_object *jso;
    time_t uptime;
    json_object_object_get_ex(root, "uptime", &jso);
    if (json_object_get_type(jso) != json_type_int)
        return;
    uptime = json_object_get_int(jso);

    __get_uptime(uptime, val);
}

static void 
get_localtime(json_object *root, char *val)
{
    json_object *jso;
    time_t local_time;
    struct tm *t;
    json_object_object_get_ex(root, "localtime", &jso);
    if (json_object_get_type(jso) != json_type_int) {
        syslog(LOG_ERR, "read localtime");
        return;
    }
    local_time = json_object_get_int(jso);
    t = gmtime((time_t*)&local_time);
    strftime(val, sizeof(char) * 40, "%Y-%m-%d %H:%M:%S", t);
}

static json_object*
get_timezone()
{
    struct strbuf sb;
    size_t i;
    json_object *jso;

    strbuf_init(&sb, 0);
    strbuf_read_file(&sb, "/tmp/TZ", 0);

    for (i = 0; sb.buf[i] != '\0' || i < sb.len; i++) {
        if (sb.buf[i] == '-') {
            sb.buf[i] = '+';
            break;
        } else if (sb.buf[i] == '+') {
            sb.buf[i] = '-';
            break;
        }
    }

    jso = json_object_new_string(sb.buf);
    strbuf_release(&sb);
    return jso;
}

static json_object*
get_memory(json_object *root)
{
    json_object *reply, *jso, *val;

    reply = json_object_new_object();
    json_object_object_get_ex(root, "memory", &jso);
    if (json_object_get_type(jso) != json_type_object) {
        syslog(LOG_ERR, "read memory");
        goto out;
    }

    json_object_object_get_ex(jso, "free", &val);
    if (json_object_get_type(val) == json_type_int) {
        JSON_ADD_INT(reply, "free", json_object_get_int(val) / 1024);
    } else {
        syslog(LOG_ERR, "read memory free");
    }
    json_object_object_get_ex(jso, "total", &val);
    if (json_object_get_type(val) == json_type_int) {
        JSON_ADD_INT(reply, "total", json_object_get_int(val) / 1024);
    } else {
        syslog(LOG_ERR, "read memory total");
    }
out:
    return reply;
}

static json_object*
get_cpu() 
{
    struct strbuf sb;
    struct strbuf **sbs;
    struct strbuf **s;
    json_object *jso = json_object_new_object();
    strbuf_init(&sb, 0);
    strbuf_exec_cmd(&sb, "top -d1 -n1 | grep '^CPU:' | sed -r s'/CPU://g'", 0);
    strbuf_trim(&sb);
    sbs = strbuf_split_buf(sb.buf, sb.len, ' ', 0);
    strbuf_release(&sb);
    s = sbs;
    while (*s) {
        char *val;
        strbuf_trim(*s);
        if ((*s)->len <= 0) {
            *s++;
            continue;
        }
        val = (*s)->buf;
        *s++;
        strbuf_trim(*s);
        if (!(*s))
            break;
        JSON_ADD_STRING(jso, (*s)->buf, val);
        *s++;
    }

    strbuf_list_free(sbs);
    return jso;
}

static json_object*
get_version()
{
    struct strbuf sb;
    json_object *jso, *reply, *value;

    jso = ubus_call("system board");
    if (jso == NULL)
        return NULL;

    reply = json_object_new_object();
    json_object_object_get_ex(jso, "kernel", &value);
    if (json_object_get_type(value) == json_type_string) {
        JSON_ADD_STRING(reply, "kernel", json_object_get_string(value));
    } else {
        syslog(LOG_ERR, "read kernel");
    }

    json_object_object_get_ex(jso, "release", &value);
    if (json_object_get_type(value) == json_type_object) {
        json_object_object_get_ex(value, "target", &value);
        if (json_object_get_type(value) == json_type_string) {
            JSON_ADD_STRING(reply, "target", json_object_get_string(value));
        } else {
            syslog(LOG_ERR, "read target");
        }
    } else {
        syslog(LOG_ERR, "read release");
    }
    json_object_put(jso);

    strbuf_init(&sb, 0);
    strbuf_exec_cmd(&sb, "cat /etc/version 2>/dev/null | awk -F= '/^DATE|^TIME/{print $2}' | tr '\\n' ' '", 0);
    strbuf_trim(&sb);
    JSON_ADD_STRING(reply, "build_date", sb.buf);
    strbuf_release(&sb);

    strbuf_exec_cmd(&sb, "cat /etc/version 2>/dev/null | awk -F= '/^BUILD=/{print $2}'", 0);
    strbuf_trim(&sb);
    JSON_ADD_STRING(reply, "build_num", sb.buf);
    strbuf_release(&sb);

    return reply;
}

static char *status_model = NULL;
static char *status_system = NULL;

void 
get_board()
{
    json_object *jso, *value;

    jso = ubus_call("system board");
    if (jso == NULL)
        return;

    if(status_model == NULL) {
        json_object_object_get_ex(jso, "model", &value);
        if (json_object_get_type(value) == json_type_string) {
            status_model = strdup(json_object_get_string(value));
        } else {
            syslog(LOG_ERR, "read model");
        }
    }
    if(status_system == NULL) {
        json_object_object_get_ex(jso, "system", &value);
        if (json_object_get_type(value) == json_type_string) {
            status_system = strdup(json_object_get_string(value));
        } else {
            syslog(LOG_ERR, "read system");
        }
    }
    json_object_put(jso);
}

json_object*
get_status()
{
    json_object *jso, *jso_reply, *jso_data;
    json_object *extra;
    char data[40];

    jso_reply = json_object_new_object();

    JSON_ADD_STRING(jso_reply, "type", "status");

    jso = ubus_call("system info");
    if (jso == NULL)
        goto out;

    jso_data = json_object_new_object();
    if (status_model == NULL || status_system == NULL) get_board();
    JSON_ADD_STRING(jso_data, "model", status_model);
    JSON_ADD_STRING(jso_data, "system", status_system);
    get_uptime(jso, data);
    JSON_ADD_STRING(jso_data, "uptime", data);
    get_localtime(jso, data);
    JSON_ADD_STRING(jso_data, "localtime", data);
    JSON_ADD_OBJECT(jso_data, "timezone", get_timezone());
    JSON_ADD_OBJECT(jso_data, "memory", get_memory(jso));
    json_object_put(jso);

    JSON_ADD_OBJECT(jso_data, "cpu", get_cpu());
    JSON_ADD_OBJECT(jso_data, "version", get_version(jso));

    extra = json_object_new_array();
    JSON_ADD_STRING_IN_ARRAY(extra, "interfaces");
    JSON_ADD_STRING_IN_ARRAY(extra, "routing");
    //JSON_ADD_STRING_IN_ARRAY(extra, "poe");
    JSON_ADD_OBJECT(jso_data, "extra", extra);

    JSON_ADD_OBJECT(jso_reply, "data", jso_data);

out:
    FREE(status_model);
    FREE(status_system);
    return jso_reply;
}

static void 
__get_inter_uptime(json_object *root, char *val)
{
    json_object *jso;
    time_t uptime;
    json_object_object_get_ex(root, "uptime", &jso);
    if (json_object_get_type(jso) != json_type_int) {
        syslog(LOG_ERR, "read uptime interface");
        return;
    }
    uptime = json_object_get_int(jso);
    __get_uptime(uptime, val);
}

static void 
__get_device_status(json_object *root, const char *intf)
{
    char cmd[124], data[40];
    float x_bytes;
    json_object *jso, *stat, *val;

    sprintf(cmd, "network.device status '{\"name\": \"%s\"}'", intf);
    jso = ubus_call(cmd);
    if (jso == NULL)
        return;

    json_object_object_get_ex(jso, "macaddr", &val);
    if (json_object_get_type(val) == json_type_string) {
        JSON_ADD_STRING(root, "mac", json_object_get_string(val));
    } else {
        syslog(LOG_ERR, "read device macaddr");
    }

    json_object_object_get_ex(jso, "statistics", &stat);
    json_object_object_get_ex(stat, "rx_bytes", &val);
    if (json_object_get_type(val) != json_type_int) {
        syslog(LOG_ERR, "read rx_bytes on interface");
        goto out;
    }
    x_bytes = (float)json_object_get_int(val);
    x_bytes /= (1024 * 1024);
    sprintf(data, "%.2f Mb", x_bytes);
    JSON_ADD_STRING(root, "rx_bytes", data);

    json_object_object_get_ex(stat, "tx_bytes", &val);
    if (json_object_get_type(val) != json_type_int) {
        syslog(LOG_ERR, "read tx_bytes on interface");
        goto out; 
    }
    x_bytes = (float)json_object_get_int(val);
    x_bytes /= (1024 * 1024);
    sprintf(data, "%.2f Mb", x_bytes);
    JSON_ADD_STRING(root, "tx_bytes", data);

out:
    json_object_put(jso);
}

static json_object*
__get_ipaddr(json_object *root)
{
    int idx, len;
    json_object *jarray;

    if (json_object_get_type(root) != json_type_array)
        return NULL;

    jarray = json_object_new_array();

    len = json_object_array_length(root);
    for (idx = 0; idx < len; idx++) {
        json_object *jso = json_object_array_get_idx(root, idx);
        json_object *address, *mask;
        char data[40];
        if(json_object_get_type(jso) != json_type_object)
            continue;
        json_object_object_get_ex(jso, "address", &address);
        if (json_object_get_type(address) != json_type_string)
            continue;
        json_object_object_get_ex(jso, "mask", &mask);
        if (json_object_get_type(mask) != json_type_int)
            continue;
        sprintf(data, "%s/%i", json_object_get_string(address),
                               json_object_get_int(mask));
        JSON_ADD_STRING_IN_ARRAY(jarray, data);
    }
    return jarray;
}

static json_object*
__get_interface(json_object *root, const char *intf)
{
    json_object *jso_reply, *value;
    jso_reply = json_object_new_object();
    char data[40];

    json_object_object_get_ex(root, "up", &value);
    if (json_object_get_type(value) != json_type_boolean)
        goto out;
    if (!json_object_get_boolean(value)) {
        JSON_ADD_BOOLEAN(jso_reply, "active", FALSE);
        goto out;
    }
    JSON_ADD_BOOLEAN(jso_reply, "active", TRUE);

    json_object_object_get_ex(root, "l3_device", &value);
    if (json_object_get_type(value) != json_type_string)
        json_object_object_get_ex(root, "device", &value);

    if (json_object_get_type(value) == json_type_string)
        JSON_ADD_STRING(jso_reply, "device", json_object_get_string(value));

    __get_inter_uptime(root, data);
    JSON_ADD_STRING(jso_reply, "uptime", data);

    json_object_object_get_ex(root, "proto", &value);
    if (json_object_get_type(value) == json_type_string)
        JSON_ADD_STRING(jso_reply, "proto", json_object_get_string(value));

    __get_device_status(jso_reply, intf);

    json_object_object_get_ex(root, "ipv4-address", &value);
    if (json_object_get_type(value) == json_type_array)
        JSON_ADD_OBJECT(jso_reply, "ipv4", __get_ipaddr(value));

    json_object_object_get_ex(root, "data", &value);
    if (json_object_get_type(value) != json_type_object)
        goto out;
    json_object_object_get_ex(value, "info", &value);
    if (json_object_get_type(value) == json_type_object) {
        enum json_type type;
        json_object_object_foreach(value, key, val) {
            type = json_object_get_type(val);
            switch(type) {
                case json_type_boolean: {
                    int v = json_object_get_boolean(val);
                    JSON_ADD_BOOLEAN(jso_reply, key, v);
                    break;
                }
                case json_type_double: {
                    int64_t v = json_object_get_double(val);
                    JSON_ADD_INT64(jso_reply, key, v);
                    break;
                }
                case json_type_int: {
                    int32_t v = json_object_get_int(val);
                    JSON_ADD_INT(jso_reply, key, v);
                    break;
                }
                case json_type_string: {
                    const char *v = json_object_get_string(val);
                    JSON_ADD_STRING(jso_reply, key, v);
                    break;
                }
                default:
                    break;
            }
        }
    }

out:
    return jso_reply;
}

static int
__ipsec_active(const char *remote)
{
    struct strbuf sb, **sbs, **s, *up = NULL;
    char data[256];
    int active = 0;
    strbuf_init(&sb, 0);
    sprintf(data, "racoonctl -l show-sa isakmp 2>/dev/null | grep '%s' | head -n 1",
            remote);
    strbuf_exec_cmd(&sb, data, 0);
    strbuf_trim(&sb);
    sbs = strbuf_split_buf(sb.buf, sb.len, ' ', 0);
    strbuf_release(&sb);
    s = sbs;
    while (*s) {
        up = *s;
        *s++;
    }
    if (up && strcmp(up->buf, "0"))
        active = 1;
    strbuf_list_free(sbs);
    return active;
}

static void
__get_ipsec_networks(json_object *root, json_object *sainfo)
{
    int idx, len;
    struct strbuf sb;
    strbuf_init(&sb, 0);
    len = json_object_array_length(sainfo);
    for (idx = 0; idx < len; idx++) {
        char data[256];
        json_object *jso, *values, *local, *remote;
        json_object *sa = json_object_array_get_idx(sainfo, idx);
        if (json_object_get_type(sa) != json_type_string)
            continue;

        sprintf(data, "uci get '{\"config\": \"racoon\", \"type\": \"sainfo\", \"section\": \"%s\"}'",
                json_object_get_string(sa));
        jso = ubus_call(data);
        printf("\n%s\n", json_object_get_string(jso));
        json_object_object_get_ex(jso, "values", &values);
        if (json_object_get_type(values) != json_type_object) {
            json_object_put(jso);
            continue;
        }

        json_object_object_get_ex(values, "local_net", &local);
        if (json_object_get_type(local) != json_type_string) {
            json_object_put(jso);
            continue;
        }

        json_object_object_get_ex(values, "remote_net", &remote);
        if (json_object_get_type(remote) != json_type_string) {
            json_object_put(jso);
            continue;
        }

        memset(data, 0, sizeof(data));
        if (sb.len == 0) {
            sprintf(data, "%s <> %s",
                    json_object_get_string(local),
                    json_object_get_string(remote));
        } else {
            sprintf(data, "\n%s <> %s",
                    json_object_get_string(local),
                    json_object_get_string(remote));
        }
        strbuf_add(&sb, data, strlen(data));
        json_object_put(jso);
    }
    JSON_ADD_STRING(root, "networks", sb.buf);
    strbuf_release(&sb);
}

static void
__get_ipsec_internal(json_object *root, json_object *jso)
{
    json_object *val, *data;
    data = json_object_new_object();
    int active = 0;

    json_object_object_get_ex(jso, "name", &val);
    if (json_object_get_type(val) == json_type_string)
        JSON_ADD_STRING(data, "name", json_object_get_string(val));

    json_object_object_get_ex(jso, "source", &val);
    if (json_object_get_type(val) == json_type_string)
        JSON_ADD_STRING(data, "source", json_object_get_string(val));
    else
        JSON_ADD_STRING(data, "source", "default");

    json_object_object_get_ex(jso, "remote", &val);
    if (json_object_get_type(val) == json_type_string) {
        JSON_ADD_STRING(data, "remote", json_object_get_string(val));
        active = __ipsec_active(json_object_get_string(val));
    }

    JSON_ADD_BOOLEAN(data, "active", active);

    json_object_object_get_ex(jso, "sainfo", &val);
    if (json_object_get_type(val) == json_type_array) {
        __get_ipsec_networks(data, val);
    }

    json_object_object_get_ex(jso, ".name", &val);
    if (json_object_get_type(val) != json_type_string)
        goto err;

    JSON_ADD_OBJECT(root, json_object_get_string(val), data);
    return;
err:
    json_object_put(data);
}

static void 
__get_ipsec(json_object *root)
{
    json_object *jso, *value;
    jso = ubus_call("uci get '{\"config\": \"racoon\", \"type\": \"tunnel\"}'");
    json_object_object_get_ex(jso, "values", &value);
    if (json_object_get_type(value) != json_type_object) {
        return;
    }

    json_object_object_foreach(value, key, val) {
        if (json_object_get_type(val) == json_type_object)
            __get_ipsec_internal(root, val);
    }

    json_object_put(jso);
}

static json_object*
__get_interfaces() 
{
    json_object *jso_reply, *jso, *jso_i;
    json_object *j_val;
    int jso_i_len, i;

    jso_reply = json_object_new_object();

    jso = ubus_call("network.interface dump");
    if (jso == NULL)
        goto out;

    json_object_object_get_ex(jso, "interface", &jso_i);
    if (json_object_get_type(jso_i) != json_type_array) {
        json_object_put(jso);
        goto out;
    }

    jso_i_len = json_object_array_length(jso_i);

    for (i = 0; i < jso_i_len; i++) {
        json_object *key;
        const char *intf;
        j_val = json_object_array_get_idx(jso_i, i);
        if (json_object_get_type(j_val) != json_type_object)
            continue;

        json_object_object_get_ex(j_val, "interface", &key);
        if (json_object_get_type(key) != json_type_string)
            continue;
        intf = json_object_get_string(key),
        JSON_ADD_OBJECT(jso_reply, intf,
                        __get_interface(j_val, intf));
    }
    __get_ipsec(jso_reply);
    json_object_put(jso);
out:
    return jso_reply;
}

json_object*
get_interfaces()
{
    json_object *jso_reply;
    jso_reply = json_object_new_object();
    JSON_ADD_STRING(jso_reply, "type", "interfaces");
    JSON_ADD_OBJECT(jso_reply, "data", __get_interfaces());

    return jso_reply;
}

static json_object*
__get_statistic(char *cmd)
{
    struct strbuf sb;
    json_object *reply;

    strbuf_init(&sb, 0);
    strbuf_exec_cmd(&sb, cmd, 0);
    reply = json_tokener_parse(sb.buf);
    strbuf_release(&sb);
    if (json_object_get_type(reply) != json_type_array) {
        json_object_put(reply);
        reply = json_object_new_array();
    }

    return reply;
}

json_object*
get_statistics(char *cmd)
{
    json_object *jso_reply;
    jso_reply = json_object_new_object();
    JSON_ADD_STRING(jso_reply, "type", "statistics");
    JSON_ADD_OBJECT(jso_reply, "data", __get_statistic(cmd));
    return jso_reply;
}

static void 
__get_uci_ifname(json_object *root, char *dev, char *ifname)
{
    json_object *jso, *value;
    int jso_len, idx;
    const char *intf;
    jso_len = json_object_array_length(root);
    for (idx = 0; idx < jso_len; idx++) {
        jso = json_object_array_get_idx(root, idx);
        if (json_object_get_type(jso) != json_type_object)
            continue;

        json_object_object_get_ex(jso, "up", &value);
        if (json_object_get_type(value) != json_type_boolean)
            continue;
        if (!json_object_get_boolean(value))
            continue;
        json_object_object_get_ex(jso, "interface", &value);
        if (json_object_get_type(value) != json_type_string)
            continue;
        intf = json_object_get_string(value);

        json_object_object_get_ex(jso, "l3_device", &value);
        if (json_object_get_type(value) == json_type_string) {
            if (!strcmp(dev, json_object_get_string(value))) {
                strcpy(ifname, intf);
                return;
            }
        }
        json_object_object_get_ex(jso, "device", &value);
        if (json_object_get_type(value) == json_type_string) {
            if (!strcmp(dev, json_object_get_string(value))) {
                strcpy(ifname, intf);
                return;
            }
        }
    }
}

static json_object*
__ip_route_balancing(json_object *intf)
{
    json_object *jarray = NULL;
    int idx, intf_len;

    jarray = json_object_new_array();

    intf_len = json_object_array_length(intf);
    for (idx = 0; idx <  intf_len; idx++) {
        json_object *jso, *value, *routes;
        int idy, route_len;
        jso = json_object_array_get_idx(intf, idx);
        if (json_object_get_type(jso) != json_type_object)
            continue;
        json_object_object_get_ex(jso, "up", &value);
        if (json_object_get_type(value) != json_type_boolean)
            continue;
        if (!json_object_get_boolean(value))
            continue;

        json_object_object_get_ex(jso, "autostart", &value);
        if (json_object_get_type(value) != json_type_boolean)
            continue;
        if (!json_object_get_boolean(value))
            continue;

        json_object_object_get_ex(jso, "route", &routes);
        if (json_object_get_type(routes) != json_type_array)
            continue;

        route_len = json_object_array_length(routes);
        for (idy = 0; idy < route_len; idy++) {
            json_object *route;
            route = json_object_array_get_idx(routes, idy);
            if (json_object_get_type(route) != json_type_object)
                continue;

            json_object_object_get_ex(route, "target", &value);
            if (json_object_get_type(value) != json_type_string) 
                continue;

            if (strcmp("0.0.0.0", json_object_get_string(value)))
                continue;

            json_object_object_get_ex(jso, "interface", &value);
            if (json_object_get_type(value) != json_type_string)
                continue;
            JSON_ADD_STRING_IN_ARRAY(jarray, json_object_get_string(value));
        }
    }
    return jarray;
}

static json_object*
__ip_route_reserving(json_object *intf)
{
    struct strbuf sb, **sbs, **sbline;
    json_object *jarray = NULL;
    strbuf_init(&sb, 0);
    strbuf_exec_cmd(&sb, "ip route list 2>/dev/null | awk '/^default/{print $5}'", 0);
    if (sb.len <= 0) {
        strbuf_release(&sb);
        return NULL;
    }

    sbline = sbs = strbuf_split_buf(sb.buf, sb.len, '\n', 0);
    strbuf_release(&sb);

    jarray = json_object_new_array();

    while(*sbline) {
        char ifname[40];
        memset(ifname, 0, 40);
        strbuf_trim((*sbline));
        __get_uci_ifname(intf, (*sbline)->buf, ifname);
        if(strlen(ifname) != 0)
            JSON_ADD_STRING_IN_ARRAY(jarray, ifname);
        *sbline++;
    }
    strbuf_list_free(sbs);
    return jarray;
}

static struct json_object*
__get_routing_info()
{
    json_object *reply = NULL;
    json_object *intf, *jso, *jbalan;
    int balans = 0;

    jso = ubus_call("network.interface dump");
    if (jso == NULL)
        return reply;
    json_object_object_get_ex(jso, "interface", &intf);
    if (json_object_get_type(intf) != json_type_array)
        goto out;

    reply = json_object_new_object();

    jbalan = ubus_call("uci get '{\"config\": \"network\", \"section\": \"globals\", \"option\": \"balancing\"}'");
    if (jbalan != NULL) {
        json_object *value;
        json_object_object_get_ex(jbalan, "value", &value);
        if (json_object_get_type(value) == json_type_int)
            balans = json_object_get_int(value);
    }
    json_object_put(jbalan);
    if (balans) {
        json_object *array;
        array = __ip_route_balancing(intf);
        if (array == NULL)
            goto out;
        JSON_ADD_STRING(reply, "mode", "balancing");
        JSON_ADD_OBJECT(reply, "interfaces", array);
    } else {
        json_object *array;
        array = __ip_route_reserving(intf);
        if (array == NULL)
            goto out;
        JSON_ADD_STRING(reply, "mode", "reserving");
        JSON_ADD_OBJECT(reply, "interfaces", array);
    }
out:
    json_object_put(jso);
    return reply;
}

void 
hex2addr(char *src, char *addr)
{
    unsigned char val[4];
    int idx;
    for (idx = 0; idx < 4; idx++) {
        sscanf(src, "%2hhx", &val[idx]);
        src += 2;
    }
    sprintf(addr, "%i.%i.%i.%i",
            val[3], val[2], val[1], val[0]);
}

struct json_object*
__get_routing()
{
    json_object *reply, *jsointf, *array;
    struct strbuf sb;
    struct strbuf **sbs, **sbline;

    reply = json_object_new_array();

    array = __get_routing_info();
    if (array != NULL)
        JSON_ADD_OBJECT_IN_ARRAY(reply, array);

    strbuf_init(&sb, 0);
    strbuf_read_file(&sb, "/proc/net/route", 0);
    sbs = strbuf_split_buf(sb.buf, sb.len, '\n', 0);
    strbuf_release(&sb);

    jsointf = ubus_call("network.interface dump");
    if (jsointf == NULL)
        goto out;

    json_object_object_get_ex(jsointf, "interface", &array);
    if (json_object_get_type(array) != json_type_array) {
        json_object_put(jsointf);
        goto out;
    }

    sbline = sbs;
    *sbline++;
    while (*sbline) {
        int idx = 0;
        char ifname[40];
        char addr[40];
        memset(ifname,0,sizeof(ifname));
        struct strbuf **sbitems, **sbitem;
        json_object *jso = json_object_new_object();
        sbitems = strbuf_split_buf((*sbline)->buf, (*sbline)->len, '\t', 0);
        sbitem = sbitems;
        while (*sbitem) {
            strbuf_trim((*sbitem));
            switch(idx) {
                case 0:
                    __get_uci_ifname(array, (*sbitem)->buf, ifname);
                    JSON_ADD_STRING(jso, "interface", ifname);
                    JSON_ADD_STRING(jso, "device", (*sbitem)->buf);
                    break;
                case 1:
                    hex2addr((*sbitem)->buf, addr);
                    JSON_ADD_STRING(jso, "dst", addr);
                    break;
                case 2:
                    hex2addr((*sbitem)->buf, addr);
                    JSON_ADD_STRING(jso, "gateway", addr);
                    break;
                case 6:
                    JSON_ADD_STRING(jso, "metric", (*sbitem)->buf);
                    break;
                case 7:
                    hex2addr((*sbitem)->buf, addr);
                    JSON_ADD_STRING(jso, "mask", addr);
                    break;
            }
            idx++;
            *sbitem++;
        }
        JSON_ADD_OBJECT_IN_ARRAY(reply, jso);
        strbuf_list_free(sbitems);
        *sbline++;
    }
    json_object_put(jsointf);
out:
    strbuf_list_free(sbs);

    return reply;
}

json_object*
get_routing()
{
    json_object *jso_reply;
    jso_reply = json_object_new_object();
    JSON_ADD_STRING(jso_reply, "type", "routing");
    JSON_ADD_OBJECT(jso_reply, "data", __get_routing());
    return jso_reply;
}
