#include "leever.h"
#include "queue.h"
#include <string.h>
#include <stdio.h>
#include <syslog.h>

#define ALLOC_CALLOC(x, nr) \
    x = calloc(1, (nr * sizeof(*(x))))

#define FREE(x) if (x) free(x); x = NULL

const bool is_cbor = true;

struct execute_msg {
	int id;
	char *command;
	char *param;
};

static struct leever_s *client = NULL;

int
leever_push(json_object *root)
{
	return queue_put(client->queue, root);
}

int
leever_empty()
{
    return queue_empty(client->queue);
}

json_object*
leever_pop()
{
    json_object *obj;
    if(queue_get(client->queue, (void**)&obj))
        return NULL;
    return obj;
}

int
leever_init(struct leever_s *leever)
{
    if (client != NULL)
        return (-1);

    if (!leever)
        return (-1);

    client = leever;

    if(net_init(&(client->net))) {
        return (-1);
    }
    client->queue = queue_create_limited(50);
    return 0;
}

void
leever_exit()
{
    if (client == NULL)
        return;
    net_done(&(client->net));
    queue_destroy(client->queue);
    client = NULL;
}

int
leever_send(json_object *root)
{
    size_t len;
    char *buf;
    int ret;

    if(json_object_get_type(root) != json_type_object)
        return -1;

    syslog(LOG_DEBUG, "send: %s", json_object_get_string(root));

    if (is_cbor) {
        cbor_stream_t stream;
        ALLOC_CALLOC(buf, 4096);
        memset(buf, 0, 4096);
        cbor_init(&stream, (unsigned char*)buf, 4096);
        cbor_dumps(&stream, root);
        len = stream.pos;
    } else {
        buf = strdup(json_object_get_string(root));
        len = strlen(buf);
    }

    if (client->id != NULL) {
        size_t len_id;
        char *msg;
        len_id = strlen(client->id);
        ALLOC_CALLOC(msg, (len + len_id + 1));
        memcpy(msg, client->id, len_id);
        memcpy(msg + len_id, "@", 1);
        memcpy(msg + len_id + 1, buf, len);
        ret = net_send(&client->net, msg, (len + len_id + 1));
        FREE(buf);
        FREE(msg);
    } else {
        ret = net_send(&client->net, buf, len);
        FREE(buf);
    }

    return ret;
}

int
push_in_leever(json_object *obj) 
{
    while(leever_push(obj))
        sleep(1);
    return 0;
}

int
push(int id, char *report, int exit_code)
{
    json_object *obj = json_object_new_object();
    json_object *data = json_object_new_object();
    json_object_object_add(obj, "type",
            json_object_new_string("execute"));

    json_object_object_add(data, "id", json_object_new_int(id));
    json_object_object_add(data, "report", json_object_new_string(report));
    json_object_object_add(data, "exit_code", json_object_new_int(exit_code));

    json_object_object_add(obj, "data", data);
    push_in_leever(obj);
    return 0;
}

int
push_s(int id, char *report, int exit_code, int status)
{
    json_object *obj = json_object_new_object();
    json_object *data = json_object_new_object();
    json_object_object_add(obj, "type",
            json_object_new_string("execute"));

    json_object_object_add(data, "id", json_object_new_int(id));
    json_object_object_add(data, "report", json_object_new_string(report));
    json_object_object_add(data, "exit_code", json_object_new_int(exit_code));
    json_object_object_add(data, "status", json_object_new_int(status));
    json_object_object_add(obj, "data", data);
    push_in_leever(obj);
    return 0;
}

void
reboot()
{
    struct strbuf sb;
    strbuf_init(&sb, 0);
    strbuf_exec_cmd(&sb, "sleep 20; reboot;", 0);
    strbuf_release(&sb);
}

void
update_fw(int id, char *param)
{
    struct strbuf sb;
    char cmd[1024];
    int ecode = 0;

    strbuf_init(&sb, 0);

    sprintf(cmd, "Downloading: %s\n", param);
    push_s(id, cmd, ecode, 1);

    sprintf(cmd, "wget %s -O /tmp/firmware", param);
    ecode = strbuf_exec_cmd(&sb, cmd, 0);
    if(ecode) {
        goto err;
    }
    strbuf_release(&sb);
    sprintf(cmd, "sysupgrade -T /tmp/firmware");
    ecode = strbuf_exec_cmd(&sb, cmd, 0);
    if (ecode) {
        goto err;
    }
    strbuf_release(&sb);
    push(id, "Updating...\n", 0);
    ecode = strbuf_exec_cmd(&sb, "sleep 10; sysupgrade /tmp/firmware", 0);
    if (ecode) {
        goto err;
    }
    strbuf_release(&sb);
    return;
err:
    push(id, sb.buf, ecode);
    strbuf_release(&sb);
}

void 
install_pkg(int id, char *param)
{
    char cmd[1024];
    int i;
    struct strbuf sb;
    char *filename;
    int ecode = 0;

    for (i = (int)strlen(param); (param[i] != '/') && (i >= 0); i--)
        filename = &(param[i]);

    sprintf(cmd, "Downloading: %s\n", param);
    push_s(id, cmd, ecode, 1);

    sprintf(cmd, "wget %s -O /opt/packages/%s", param, filename);
    ecode = strbuf_exec_cmd(&sb, cmd, 0);
    if (ecode) {
        goto err;
    }
    strbuf_release(&sb);
    sprintf(cmd, "opkg --noaction install /opt/packages/%s", filename);
    ecode = strbuf_exec_cmd(&sb, cmd, 0);
    if (ecode) {
        goto err;
    }
    strbuf_release(&sb);
    sprintf(cmd, "opkg install /opt/packages/%s", filename);
    ecode = strbuf_exec_cmd(&sb, cmd, 0);
    if (ecode) {
        goto err;
    }
    strbuf_release(&sb);
    return;
err:
    push(id, sb.buf, ecode);
    strbuf_release(&sb);
}

void
custom(int id, char *param)
{
    int ecode;
    size_t i;
    struct strbuf sb;
    char buf[1025];
    strbuf_init(&sb, 0);
    ecode = strbuf_exec_cmd(&sb, param, 0);
    if (sb.len > 1024) {
        for (i = 0; i < sb.len;) {
            size_t size = (1024 > (sb.len - i)) ? (size_t)(sb.len - i): 1024;
            memset(buf, 0, 1025);
            memcpy(buf, sb.buf+i, size);
            push(id, buf, ecode);
            i += size;
        }
    } else {
        push(id, sb.buf, ecode);
    }
    strbuf_release(&sb);
}

void*
__execute_run(void *args)
{
    struct execute_msg *msg = (struct execute_msg*)args;

    if (!strcmp(msg->command, "reboot")) {
        reboot();
    } else if (!strcmp(msg->command, "update_fw")) {
        update_fw(msg->id, msg->param);
    } else if (!strcmp(msg->command, "install_pkg")) {
        install_pkg(msg->id, msg->param);
    } else if (!strcmp(msg->command, "custom")) {
        custom(msg->id, msg->param);
    }
    free(msg->command);
    free(msg->param);
    free(msg);
    pthread_exit(0);
}

int
execute_run(json_object *obj)
{
    struct execute_msg *msg;
    json_object *val;
    pthread_t thread;
    msg = calloc(1, sizeof(*msg));

    json_object_object_get_ex(obj, "id", &val);
    if (json_object_get_type(val) != json_type_int)
        goto err;
    msg->id = json_object_get_int(val);
    json_object_object_get_ex(obj, "command", &val);
    if (json_object_get_type(val) != json_type_string)
        goto err;
    msg->command = strdup(json_object_get_string(val));

    json_object_object_get_ex(obj, "param", &val);
    if (json_object_get_type(val) == json_type_string)
        msg->param = strdup(json_object_get_string(val));

    pthread_create(&thread, NULL, __execute_run, (void*)msg);
    pthread_detach(thread);
    return 0;
err:
    free(msg);
    return -1;
}

int
parse_answer(json_object *root)
{
    json_object *val = NULL;
    if (!root)
        return -1;

    json_object_object_get_ex(root, "status", &val);
    if (json_object_get_type(val) != json_type_string)
        return -1;
    if (strcmp(json_object_get_string(val), "accept"))
        return -1;
    syslog(LOG_DEBUG, "recv package accept");
    json_object_object_get_ex(root, "execute", &val);
    if(val && (json_object_get_type(val) == json_type_object)) {
        execute_run(val);
    }

    return 0;
}

int
leever_recv()
{
    int ret;
    char *buf;
    struct strbuf sb;
    json_object *jso;
    strbuf_init(&sb, 0);
    ALLOC_CALLOC(buf, 4096);
    strbuf_attach(&sb, buf, 4096, 4096);
    ret = (int)net_recv(&client->net, sb.buf, sb.len);
    if (ret <= 0) {
        strbuf_release(&sb);
        return ret;
    }

    if (is_cbor) {
        cbor_stream_t stream;
        cbor_init(&stream, (unsigned char*)sb.buf, (size_t)ret);
        jso = cbor_loads(&stream);
    } else {
        jso = json_tokener_parse(sb.buf);
    }
    if (json_object_get_type(jso) == json_type_object)
        if (parse_answer(jso))
            ret = -1;

    json_object_put(jso);
    strbuf_release(&sb);
    return ret;
}

void
leever_loop()
{
    json_object *last = NULL;
    int attempt, ret, i;
    for (i = 0; i < 304; i++) {
        if (last == NULL) {
            if ((last = leever_pop()) == NULL) {
                sleep(1);
                continue;
            }
        }
        attempt = 0;
        while (attempt < client->resend_attempts) {
            attempt++;
            leever_send(last);
            ret = leever_recv();
            if (ret != 0) {
                json_object_put(last);
                last = NULL;
                break;
            } else {
                syslog(LOG_WARNING, "attempts timeout");
                sleep((unsigned)attempt);
                continue;
            }
        }
    }
}
