#ifndef _QUEUE_H_
#define _QUEUE_H_

#include <pthread.h>
#include <stdint.h>

struct queue_element_s {
    void *data;
    struct queue_element_s *next;
};

struct queue_s {
    struct queue_element_s *first_el, *last_el;
    uint32_t num_el;
    uint32_t max_el;
    pthread_mutex_t *mutex;
};


/**
 * initialized and allocates a queue with unlimited elements
 *
 * return NULL on error, or a pointer to the queue
 */
struct queue_s *queue_create(void);

/**
 * initialized and allocates a queue
 * 
 * max_elements - maximum number of element
 *
 * return NULL on error, or a pointer to the queue
 */
struct queue_s *queue_create_limited(uint32_t max_elements);

/**
 * releases the memory internally allocated and destroy the queue
 * you have to release the memory the elements in the queue use
 *
 * q - the queue to be destroyed
 *
 * returns 0 if everything worked, < 0 if error
 */
int8_t queue_destroy(struct queue_s *q);

/**
 * returns wether the queue is empty
 *
 * q - the queue
 *
 * returns zero if queue is NOT empty, < 0 => error
 */
int8_t queue_empty(struct queue_s *q);

/**
 * put a new element at the end of the queue
 *
 * q - the queue
 * e - element
 *
 * returns 0 if everything worked, > 0 if max_elements is reached, < 0 if error
 */
int8_t queue_put(struct queue_s *q, void *e);

/**
 * get the firsy elements of the queue
 *
 * q - the queue
 * e - pointer which will be set to the element
 *
 * returns 0 if everything worked, > 0 if not elements in queue, < 0 if error
 */
int8_t queue_get(struct queue_s *q, void **e);

#endif /* QUEUE_H_ */
