#ifndef _LEEVER_H_
#define _LEEVER_H_

#include <pthread.h>
#include "net.h"
#include "strbuf.h"
#include "cbor.h"

struct queue_s;

struct leever_s {
    struct net_s net;

    char *id;

    struct queue_s *queue;

    int resend_attempts;
};

int leever_push(json_object *);
int leever_empty();
json_object *leever_pop();

int leever_init(struct leever_s *);
void leever_loop();
void leever_exit();

int leever_send(json_object *);
int leever_recv();//struct strbuf *);

#endif
