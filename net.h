#ifndef _NET_H_
#define _NET_H_

#include <arpa/inet.h>

struct net_s {
	char *addr;
	uint16_t port;
	/* recv_timeout in seconds */
	int recv_timeout;

	int sock;
	struct sockaddr_in addr_in;
};

int net_init(struct net_s*);
void net_done(struct net_s*);

int net_send(struct net_s*, char*, size_t);
int net_recv(struct net_s*, char*, size_t);

#endif
