#include "queue.h"
#include <stdlib.h>

int8_t queue_lock(struct queue_s *q) 
{
    if (q == NULL)
        return -1;
    if (0 != pthread_mutex_lock(q->mutex))
        return -1;
    return 0;
}

int8_t queue_unlock(struct queue_s *q)
{
    if (q == NULL)
        return -1;
    if (0 != pthread_mutex_unlock(q->mutex))
       return -1;
   return 0; 
}

struct queue_s *queue_create(void) 
{
    struct queue_s *q = calloc(1, sizeof(struct queue_s));
    if (q != NULL) {
        q->mutex = (pthread_mutex_t*)calloc(1, sizeof(pthread_mutex_t));
        if (q->mutex == NULL) {
            free(q);
            return NULL;
        }
        pthread_mutex_init(q->mutex, NULL);

        q->first_el = NULL;
        q->last_el = NULL;
        q->num_el = 0;
        q->max_el = 0;
    }
    return q;
}

struct queue_s *queue_create_limited(uint32_t max_elements)
{
    struct queue_s *q = queue_create();
    if (q != NULL)
        q->max_el = max_elements;

    return q;
}

int8_t queue_destroy(struct queue_s *q)
{
    if (q == NULL)
        return -1;
    if (0 != queue_lock(q))
        return -1;

    if (q->first_el != NULL || q->last_el != NULL) {
        struct queue_element_s *item;
        while(q->first_el) {
            item = q->first_el;
            q->first_el = item->next;
            free(item);
        }
    }

    pthread_mutex_destroy(q->mutex);
    free(q->mutex);
    free(q);
    return 0;
}

int8_t queue_empty(struct queue_s *q)
{
    if (q == NULL)
        return -1;
    if (0 != queue_lock(q))
        return -1;

    int8_t ret;
    if (q->first_el == NULL || q->last_el == NULL)
        ret = 1;
    else
        ret = 0;
    if (0 != queue_unlock(q))
        return -1;
    return ret;
}

int8_t queue_put_internal(struct queue_s *q, void *el)
{
    if (q->max_el != 0 && q->num_el >= q->max_el) {
        return 1;
    }
    struct queue_element_s *new_el = calloc(1, sizeof(struct queue_element_s));
    if (new_el == NULL) {
        return -1;
    }
    new_el->data = el;
    new_el->next = NULL;
    if (q->first_el == NULL || q->last_el == NULL) {
        q->first_el = new_el;
        q->last_el = new_el;
    } else {
        q->last_el->next = new_el;
        q->last_el = new_el;
    }
    q->num_el++;
    return 0;
}

int8_t queue_put(struct queue_s *q, void *el) 
{
    if (q == NULL)
        return -1;
    if (0 != queue_lock(q))
        return -1;

    int8_t ret = queue_put_internal(q, el);

    if (0 != queue_unlock(q))
        return -1;
    return ret;
}

int8_t queue_get_internal(struct queue_s *q, void **e)
{
    *e = NULL;
    if (q == NULL) {
        return -1;
    }
    if (q->num_el == 0) {
        return 1;
    }
    // get first element
    struct queue_element_s *item = q->first_el;
    if (item != NULL) {
        q->first_el = item->next;
        if (q->first_el == NULL)
            q->last_el = NULL;
        q->num_el--;
        *e = item->data;
        free(item);
    } else {
        return 1;
    }
    return 0;
}

int8_t queue_get(struct queue_s *q, void **e)
{
    *e = NULL;
    if (q == NULL)
        return -1;
    if (0 != queue_lock(q))
        return -1;

    int8_t ret = queue_get_internal(q, e);

    if (0 != queue_unlock(q))
        return -1;
    return ret;
}
